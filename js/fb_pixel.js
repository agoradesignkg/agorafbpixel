/**
 * @file
 * Initialize FB pixel on the page.
 */

(function (drupalSettings) {
  'use strict';

  if (!drupalSettings.agorafbpixel) {
    return;
  }
  var afbSettings = drupalSettings.agorafbpixel;

  /*eslint-disable */
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
  /*eslint-enable */

  fbq('init', afbSettings.facebook_id);
  fbq('track', 'PageView');

  for (var i = 0; i < afbSettings.commands.length; i++) {
    var command = afbSettings.commands[i];
    fbq(command.command, command.event, command.data);
  }

})(drupalSettings);
