<?php

namespace Drupal\agorafbpixel_commerce\EventSubscriber;

use Drupal\agorafbpixel\CommandRegistryInterface;
use Drupal\agorafbpixel\GenericFbPixelCommand;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines the cart subscriber.
 *
 * It will track the AddToCart Facebook event.
 */
class CartSubscriber implements EventSubscriberInterface {

  /**
   * The Facebook Pixel command registry.
   *
   * @var \Drupal\agorafbpixel\CommandRegistryInterface
   */
  protected $commandRegistry;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      CartEvents::CART_ENTITY_ADD => 'onAddToCart',
    ];
    return $events;
  }

  /**
   * Constructs a new CartSubscriber object.
   *
   * @param \Drupal\agorafbpixel\CommandRegistryInterface $command_registry
   *   The command registry service.
   */
  public function __construct(CommandRegistryInterface $command_registry) {
    $this->commandRegistry = $command_registry;
  }

  /**
   * Add to cart event.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The cart entity add event.
   */
  public function onAddToCart(CartEntityAddEvent $event) {
    // This isn't needed actually, as otherwise the commands would never get
    // sent, but there is also no need to create unused commands.
    if (!agorafbpixel_is_tracking_enabled()) {
      return;
    }

    $order_item = $event->getOrderItem();
    $purchasable_entity = $event->getEntity();
    $item_id = $purchasable_entity instanceof ProductVariationInterface ? $purchasable_entity->getSku() : $purchasable_entity->id();

    $command_data = [
      'value' => floatval($order_item->getUnitPrice()->getNumber()),
      'currency' => $order_item->getTotalPrice()->getCurrencyCode(),
      'content_type' => 'product',
      'content_ids' => [$item_id],
      'content_name' => $order_item->getTitle(),
      'contents' => [
        [
          'id' => $item_id,
          'quantity' => $order_item->getQuantity(),
        ],
      ],
    ];
    $command = new GenericFbPixelCommand('AddToCart', $command_data);
    $this->commandRegistry->addDelayedCommand($command);
  }


}
