<?php

namespace Drupal\agorafbpixel_commerce\EventSubscriber;

use Drupal\agorafbpixel\CommandRegistryInterface;
use Drupal\agorafbpixel\GenericFbPixelCommand;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ViewContentSubscriber implements EventSubscriberInterface {

  /**
   * The Facebook Pixel command registry.
   *
   * @var \Drupal\agorafbpixel\CommandRegistryInterface
   */
  protected $commandRegistry;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new ViewContentSubscriber object.
   *
   * @param \Drupal\agorafbpixel\CommandRegistryInterface $command_registry
   *   The command registry service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(CommandRegistryInterface $command_registry, RouteMatchInterface $route_match) {
    $this->commandRegistry = $command_registry;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      KernelEvents::REQUEST => ['onKernelRequest'],
    ];
    return $events;
  }

  /**
   * On product pages, the "ViewContent" event gets fired.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event to process.
   */
  public function onKernelRequest(GetResponseEvent $event) {
    // This isn't needed actually, as otherwise the commands would never get
    // sent, but there is also no need to create unused commands.
    if (!agorafbpixel_is_tracking_enabled()) {
      return;
    }

    $request = $event->getRequest();
    $route_name = $this->routeMatch->getRouteName();

    switch ($route_name) {
      case 'commerce_checkout.form':
        $step = $this->routeMatch->getRawParameter('step');
        // @todo make this more flexible to custom checkout flows.
        if ($step == 'order_information') {
          // @todo add currency, value, num_items at least.
          $command = new GenericFbPixelCommand('InitiateCheckout');
          $this->commandRegistry->addCommand($command);
        }
        break;

      case 'entity.commerce_product.canonical':
        /** @var \Drupal\commerce_product\Entity\ProductInterface $entity */
        $entity = $this->routeMatch->getParameter('commerce_product');
        $command_data = [
          'content_name' => $entity->getTitle(),
          'content_ids' => [$entity->id()],
        ];
        $command = new GenericFbPixelCommand('ViewContent', $command_data);
        $this->commandRegistry->addCommand($command);
        break;
    }
  }

}
