<?php

namespace Drupal\agorafbpixel_commerce\EventSubscriber;

use Drupal\agorafbpixel\CommandRegistryInterface;
use Drupal\agorafbpixel\GenericFbPixelCommand;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines the purchase tracking subscriber.
 *
 * This event subscriber acts on the place transition of commerce order
 * entities, in order to generate and set an order number.
 */
class PurchaseTrackingSubscriber implements EventSubscriberInterface {

  /**
   * The Facebook Pixel command registry.
   *
   * @var \Drupal\agorafbpixel\CommandRegistryInterface
   */
  protected $commandRegistry;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['trackPurchase'],
    ];
    return $events;
  }

  /**
   * Constructs a new PurchaseTrackingSubscriber object.
   *
   * @param \Drupal\agorafbpixel\CommandRegistryInterface $command_registry
   *   The command registry service.
   */
  public function __construct(CommandRegistryInterface $command_registry) {
    $this->commandRegistry = $command_registry;
  }

  /**
   * Sends the purchase event to the Facebook Pixel.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function trackPurchase(WorkflowTransitionEvent $event) {
    // This isn't needed actually, as otherwise the commands would never get
    // sent, but there is also no need to create unused commands.
    if (!agorafbpixel_is_tracking_enabled()) {
      return;
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    $command_data = [
      'value' => floatval($order->getTotalPrice()->getNumber()),
      'currency' => $order->getTotalPrice()->getCurrencyCode(),
    ];
    $command = new GenericFbPixelCommand('Purchase', $command_data);
    $this->commandRegistry->addDelayedCommand($command);
  }

}
