<?php

namespace Drupal\agorafbpixel;

/**
 * Defines the Facebook Tracking pixel command interface.
 */
interface FbPixelCommandInterface {

  /**
   * Get the command name.
   *
   * @return string
   *   The command name.
   */
  public function getCommand();

  /**
   * Get the event name.
   *
   * @return string
   *   The event name.
   */
  public function getEvent();

  /**
   * Get the data.
   *
   * @return array
   *   The data.
   */
  public function getData();

  /**
   * An integer value for sorting by priority.
   *
   * @return int
   *   The priority value.
   */
  public function getPriority();

  /**
   * Get an array representation of the command.
   *
   * @return array
   *   The tracking command as array.
   */
  public function toArray();

}
