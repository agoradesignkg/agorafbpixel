<?php

namespace Drupal\agorafbpixel;

/**
 * Defines the command registry interface.
 */
interface CommandRegistryInterface {

  /**
   * Add a command to the registry.
   *
   * @param \Drupal\agorafbpixel\FbPixelCommandInterface $command
   *   An analytics command.
   */
  public function addCommand(FbPixelCommandInterface $command);

  /**
   * Add a delayed command to the registry.
   *
   * Delayed command will be sent on the following page request.
   *
   * @param \Drupal\agorafbpixel\FbPixelCommandInterface $command
   *   An analytics command.
   */
  public function addDelayedCommand(FbPixelCommandInterface $command);

  /**
   * Get all commands registered.
   *
   * @return \Drupal\agorafbpixel\FbPixelCommandInterface[]
   *   The array of registered commands.
   */
  public function getCommands();

  /**
   * Format the commands for use in drupalSettings.
   *
   * @return array
   *   An array of commands for use in drupalSettings.
   */
  public function getDrupalSettingCommands();

}
