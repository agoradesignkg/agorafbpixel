<?php

namespace Drupal\agorafbpixel;

/**
 * Defines the generic Facebook Pixel command class.
 */
class GenericFbPixelCommand implements FbPixelCommandInterface {

  /**
   * The default priority.
   *
   * @var int
   */
  const DEFAULT_PRIORITY = 0;

  /**
   * The command name.
   *
   * @var string
   */
  protected $command;

  /**
   * The event name.
   *
   * @var string
   */
  protected $event;

  /**
   * The data.
   *
   * @var array
   */
  protected $data;

  /**
   * The priority.
   *
   * @var int
   */
  protected $priority;

  /**
   * Constructs a new GenericFbPixelCommand object.
   *
   * @param string $event
   *   The event name.
   * @param array $data
   *   The command data. Defaults to an empty array.
   * @param int $priority
   *   The priority.
   * @param string $command
   *   The command name. Defaults to 'track'.
   */
  public function __construct($event, array $data = [], $priority = self::DEFAULT_PRIORITY, $command = 'track') {
    $this->command = $command;
    $this->event = $event;
    $this->data = $data;
    $this->priority = $priority;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommand() {
    return $this->command;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriority() {
    return $this->priority;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return [
      'command' => $this->getCommand(),
      'event' => $this->getEvent(),
      'data' => $this->getData(),
    ];
  }

}
